import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  constructor(
    private http: HttpClient
  ) { }

  registrar$(data) {
    return this.http.post(
      'http://159.89.81.201/api/usuarios',
      JSON.stringify(data), {
        headers: {
          'Content-Type': 'application/json'
        }
      });
  }

}
