import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../shared/services/toast.service';
import { HttpRequestService } from './services/http-request.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  constructor(
    private toastService: ToastService,
    private httpService: HttpRequestService,
    private navController: NavController,
  ) { }

  formInstance: FormGroup;

  ngOnInit() {

    this.formInstance = new FormGroup({
      nombres: new FormControl('', Validators.required),
      apellidos: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      repeatPassword: new FormControl('', Validators.required),
    });

  }

  async registrar() {

    const dataForm = this.formInstance.getRawValue();

    for (let attr in dataForm) {
      if (this.formInstance.get(attr).invalid) {
        this.toastService.presentToast('Campo requerido: ' + attr);
        return;
      }
    }

    const { repeatPassword, ...json } = dataForm;
    if (json.password !== repeatPassword) {
      this.toastService.presentToast('Los campos de contraseña deben coincidir');
      return;
    }

    try {
      const response = await this.httpService.registrar$({ ...json, id_tipo_usuario: 1 }).toPromise();

      if (response['status'] === 'success') {
        this.toastService.presentToast('Registro exitoso.');
        this.navController.back();
      } else if (response['status'] === 'fail') {
        throw new Error(response['error']);
      } else if (response['errors']) {
        throw new Error(response['errors'][0]);
      } else {
        throw new Error('Algo salio mal');
      }

    } catch (error) {
      this.toastService.presentToast(error);
    } finally {

    }

  }

}
