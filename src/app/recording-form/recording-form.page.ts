import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { Storage } from '@ionic/storage';
import { ToastService } from '../shared/services/toast.service';
import { NavController } from '@ionic/angular';
import { HttpRequestService } from './services/http-request.service';

import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-recording-form',
  templateUrl: './recording-form.page.html',
  styleUrls: ['./recording-form.page.scss'],
})
export class RecordingFormPage implements OnInit {

  @ViewChild('audioFile', { static: false }) audioFile: any;

  constructor(
    private toastService: ToastService,
    private file: File,
    private storage: Storage,
    private navController: NavController,
    private httpService: HttpRequestService,
  ) { }

  formInstance: FormGroup;

  audio: any;

  user: any;

  titulo: string;

  ngOnInit() {

    this.storage.get('user')
      .then((user) => {
        this.user = user;
      });

    this.formInstance = new FormGroup(
      {
        titulo: new FormControl('', Validators.required),
        genero: new FormControl('', Validators.required),
        tonalidad: new FormControl('', Validators.required),
        compas: new FormControl('', Validators.required),
        letra: new FormControl('', Validators.required),
      });

    this.storage.get('audioSelected')
      .then((audio: any) => {
        this.audio = audio;

        if (audio.id_grabacion) {
          this.titulo = audio.titulo || audio.nombre;
          this.formInstance.get('titulo').setValue(audio.titulo);
          this.formInstance.get('genero').setValue(audio.genero);
          this.formInstance.get('tonalidad').setValue(audio.tonalidad);
          this.formInstance.get('compas').setValue(audio.compas);
          this.formInstance.get('letra').setValue(audio.letra);

        } else {
          this.titulo = 'Registrar audio';
        }
      });
  }

  async registrarGrabacion() {

    const dataForm = this.formInstance.getRawValue();

    for (let attr in dataForm) {
      if (this.formInstance.get(attr).invalid) {
        debugger
        this.toastService.presentToast(`Campo requerido: ${attr}`);
        return;
      }
    }

    if (this.audio.id_grabacion) {

      let deviceList = await this.storage.get('listaAudios') || [];

      deviceList = deviceList.map((au) => {
        if (au.id_grabacion === this.audio.id_grabacion) {
          return {
            ...au,
            ...dataForm
          };
        } else {
          return au;
        }
      });

      this.storage.set('listaAudios', deviceList);

      this.toastService.presentToast(`Grabacion editada`);
      this.navController.back();

    } else {

      try {
        const fullPath: string = this.audio[0].fullPath;

        const fileName = this.audio[0].name;

        const path = fullPath.substr(0, fullPath.lastIndexOf('/'));

        const ab = await this.file.readAsArrayBuffer(path, fileName);
        const formData = new FormData();

        formData.append('archivo', new Blob([ab]), this.audio[0].name);
        formData.append('id_tipo_grabacion', '1');
        formData.append('id_usuario', this.user.id_usuario || this.user.userId);

        for (let attr in dataForm) {
          formData.append(attr, dataForm[attr]);
        }

        const response = await this.httpService.registrarGrabacion$(formData).toPromise();

        if (response['status'] === 'success') {

          const audioList = await this.storage.get('listaAudios') || [];

          audioList.push({
            ...dataForm,
            ...response['data']
          });

          this.storage.set('listaAudios', audioList);

          this.toastService.presentToast(`Grabacion registrada`);
          this.navController.back();
        } else {
          throw new Error('Algo salió mal');
        }

      } catch (err) {
        this.toastService.presentToast(err);
      }

    }

  }

}
