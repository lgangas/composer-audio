import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  constructor(private httpClient: HttpClient) { }

  registrarGrabacion$(formData: FormData) {
    return this.httpClient.post('http://159.89.81.201/api/grabaciones', formData);
  }
}
