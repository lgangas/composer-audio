import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { RecordingFormPage } from './recording-form.page';
import { SharedModule } from '../shared/shared.module';

import { File } from '@ionic-native/file/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { HttpRequestService } from './services/http-request.service';

const routes: Routes = [
  {
    path: '',
    component: RecordingFormPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,
    HttpClientModule,
  ],
  declarations: [RecordingFormPage],
  providers: [
    HttpRequestService,
    File,
  ]
})
export class RecordingFormPageModule { }
