import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AudioSelectedService {

  constructor() { }

  private audioSelected = new BehaviorSubject(null);

  audioSelected$ = this.audioSelected.asObservable();

  emitAudioSelected(audio: any) {
    this.audioSelected.next(audio);
  }

}
