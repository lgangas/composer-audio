import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AudioListService {

  constructor() { }

  private audioList = new BehaviorSubject([]);

  audioList$ = this.audioList.asObservable();

  emitAudioList(list) {
    this.audioList.next(list);
  }

}
