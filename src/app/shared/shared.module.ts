import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AudioSelectedService } from './services/audio-selected.service';
import { ToastController, NavController } from '@ionic/angular';
import { ToastService } from './services/toast.service';
import { AudioListService } from './services/audio-list.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
  ],
  providers: [
    AudioSelectedService,
    ToastService,
    ToastController,
    NavController,
    AudioListService,
  ]
})
export class SharedModule { }
