import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from '../shared/services/toast.service';
import { HttpRequestService } from './services/http-request.service';
import { NavController } from '@ionic/angular';
<<<<<<< HEAD
import { Storage } from '@ionic/storage'
=======
import { Storage } from '@ionic/storage';
>>>>>>> e23a61d4139681abbc386403c9a447276ed9ea5f
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { HttpRequestService as RegisterService } from '../register/services/http-request.service';
import { HttpRequestService as HomeService } from '../home/services/http-request.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    private toasteService: ToastService,
    private httpService: HttpRequestService,
    private navController: NavController,
    private storage: Storage,
    private googlePlus: GooglePlus,
    private registerService: RegisterService,
    private homeService: HomeService,
    private router: Router
  ) { }

  formInstace: FormGroup;

  ngOnInit() {
    this.formInstace = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });

  }

  async ingresarGoogle() {

    try {
      const res = await this.googlePlus.login({})

      const googleUsers = await this.storage.get('googleUsers') || [];

      const existUser = googleUsers.find((gu) => gu.userId === res.userId);

      if (!existUser) {
        const json = {
          nombres: res.givenName,
          apellidos: res.familyName,
          email: res.email,
          password: res.userId,
          id_tipo_usuario: 1
        };

        const responseRegister = await this.registerService.registrar$(json).toPromise();

        if (responseRegister['status'] === 'success') {
          googleUsers.push({ ...responseRegister['data'][0], userId: res.userId });
          this.storage.set('googleUsers', googleUsers);
          this.goHome({ ...responseRegister['data'][0], userId: res.userId }, res.givenName);
        } else {
          this.googlePlus.logout();
        }

      } else {
        this.goHome({ ...existUser, userId: res.userId }, res.givenName);
      }

    } catch (error) {
<<<<<<< HEAD
      console.log({error});
=======
>>>>>>> e23a61d4139681abbc386403c9a447276ed9ea5f
      if (error !== 12501) {
        this.toasteService.presentToast(error);
      }
    }

  }

  async ingresar() {

    const dataForm = this.formInstace.getRawValue();

    for (let attr in dataForm) {
      if (this.formInstace.get(attr).invalid) {
        this.toasteService.presentToast('Campo requerido: ' + attr);
        return;
      }
    }

    try {
      const response = await this.httpService.login$(dataForm).toPromise();

      if (response['status'] === 'fail') {
        throw new Error(response['error']);
      }

      this.goHome(response['data'][0], response['data'][0].nombres);

    } catch (error) {
      this.toasteService.presentToast(error);
    } finally {

    }

  }

  async goHome(data: any, name: string) {
    this.storage.set('user', data)
    this.toasteService.presentToast(`Hola ${name}`);
    this.router.navigateByUrl('/home');
  }

}
