import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule, NavController } from '@ionic/angular';

import { LoginPage } from './login.page';
import { SharedModule } from '../shared/shared.module';
import { HttpRequestService } from './services/http-request.service';
import { HttpRequestService as RegisterService } from '../register/services/http-request.service';
import { HttpRequestService as HomeService } from '../home/services/http-request.service';
import { HttpClientModule } from '@angular/common/http';

import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { IonicStorageModule } from '@ionic/storage';

const routes: Routes = [
  {
    path: '',
    component: LoginPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    SharedModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [LoginPage],
  providers: [
    HttpRequestService,
    RegisterService,
    HomeService,
    GooglePlus,
  ]
})
export class LoginPageModule { }
