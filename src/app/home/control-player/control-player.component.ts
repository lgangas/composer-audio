import { Component, OnInit, Input, OnChanges, SimpleChanges, ElementRef, ViewChild, Output, EventEmitter } from '@angular/core';
import { MediaObject, Media } from '@ionic-native/media/ngx';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
// import { AudioProvider } from 'ionic-audio';

@Component({
  selector: 'app-control-player',
  templateUrl: './control-player.component.html',
  styleUrls: ['./control-player.component.scss'],
  animations: [
    trigger('changeHeight', [
      state('*', style({
        height: '0'
      })),
      state('initial', style({
        height: '40vh'
      })),
      state('final', style({
        height: '80px'
      })),
      transition('*=>initial', animate('300ms')),
      transition('initial=>final', animate('300ms')),
    ]),
  ]
})
export class ControlPlayerComponent implements OnInit, OnChanges {

  position = '*';


  constructor(
    private media: Media,
    // private audioProvider: AudioProvider
  ) { }

  ngOnInit() {
    this.position = 'initial';
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.audio && changes.audio.currentValue) {
      const ms = new Date(0, 0).setSeconds(changes.audio.currentValue.duration);
      this.duration = new Date(ms);
      this.letra = this.audio.letra;
      this.selectAudio();
    }
  }

  @ViewChild('hiddenButton', { static: true }) hiddeButton: ElementRef;

  @Input() audio: any;

  @Output() stopPlayingEmitter = new EventEmitter();

  isPlaying = false;

  currentAudio: MediaObject;

  currentAudioSubscription = new Subscription();

  statusAudio = 0;

  duration: Date;

  letra = `Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores commodi totam voluptatem quod, porro
    quasi ipsum, rerum rem architecto error est consequuntur! Provident, cum! Labore maxime esse blanditiis
    beatae atque?`;

  async selectAudio() {
    this.stopPlaying();
    this.currentAudio = this.media.create(environment.host + this.audio.ruta);
    this.toggleAudio();
  }

  stopPlaying() {
    this.isPlaying = false;
    this.currentAudioSubscription.unsubscribe();

    if (this.currentAudio) {
      this.currentAudio.stop();
    }
  }

  toggleAudio() {

    if (this.isPlaying) {
      this.isPlaying = false;
      this.currentAudio.pause();
    } else {
      this.isPlaying = true;
      this.currentAudio.play()

      if (this.currentAudioSubscription.closed) {
        this.currentAudioSubscription = this.currentAudio.onStatusUpdate
          .subscribe((res) => {
            if (res === 4) {
              this.hiddeButton.nativeElement.click();
              this.stopPlayingEmitter.emit(true);
            }
          });
      }
    }

  }

  changeAnimation() {
    if (this.position === 'initial') {
      this.position = 'final';
    } else if (this.position === 'final') {
      this.position = 'initial';
    }
  }

}
