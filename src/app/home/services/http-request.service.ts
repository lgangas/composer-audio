import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HttpRequestService {

  constructor(private httpClient: HttpClient) { }

  listarGrabaciones$(filtros) {
    return this.httpClient.get('http://159.89.81.201/api/grabaciones', {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  obtenerUsuario(id: number) {
    return this.httpClient.get('http://159.89.81.201/api/usuarios/' + id, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

}
