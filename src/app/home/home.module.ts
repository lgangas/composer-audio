import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule, PopoverController } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { GooglePlus } from '@ionic-native/google-plus/ngx';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { Media } from '@ionic-native/media/ngx';
import { File } from '@ionic-native/file/ngx';

import { MediaCapture } from '@ionic-native/media-capture/ngx';

import { IonicStorageModule } from '@ionic/storage';
import { ControlPlayerComponent } from './control-player/control-player.component';
import { PopoverComponent } from './popover/popover.component';
import { HttpRequestService } from './services/http-request.service';


import { HomePage } from './home.page';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HttpClientModule,
    SharedModule,
    RouterModule.forChild([
      {
        path: '',
        component: HomePage
      }
    ])
  ],
  entryComponents: [
    PopoverComponent
  ],
  declarations: [
    HomePage,
    ControlPlayerComponent,
    PopoverComponent,
  ],
  providers: [
    GooglePlus,
    SocialSharing,
    Media,
    File,
    MediaCapture,
    PopoverController,
    HttpRequestService
  ]
})
export class HomePageModule { }
