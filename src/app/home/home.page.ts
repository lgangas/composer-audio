import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { GooglePlus } from '@ionic-native/google-plus/ngx';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { Media, MediaObject } from '@ionic-native/media/ngx';

import { File } from '@ionic-native/file/ngx';

import { MediaCapture, MediaFile, CaptureError } from '@ionic-native/media-capture/ngx';
import { Storage } from '@ionic/storage';
import { HttpRequestService } from './services/http-request.service';
import { NavController, PopoverController, MenuController } from '@ionic/angular';
import { PopoverComponent } from './popover/popover.component';
import { environment } from 'src/environments/environment';
import { ToastService } from '../shared/services/toast.service';
import { Subject, Subscription } from 'rxjs';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  @ViewChild('hiddenButton', { static: false }) hiddenButton: ElementRef;

  constructor(
    private googlePlus: GooglePlus,
    private socialSharing: SocialSharing,
    private media: Media,
    private file: File,
    private mediaCapture: MediaCapture,
    private storage: Storage,
    private httpService: HttpRequestService,
    private navController: NavController,
    private popoverController: PopoverController,
    private toastService: ToastService,
    private menuController: MenuController
  ) { }

  private closerSubscriber = new Subscription();

  audioSelectedIndex: number;

  isRecording = false;

  isPlaying = false;

  ngOnInit() { }

  mediaFiles = [];

  totalAudios = [];

  audio: any;

  filter = {
    genero: '',
    tonalidad: '',
    compas: ''
  };

  ionViewWillEnter() {

    this.audio = null;

    this.audioSelectedIndex = null;

    this.isPlaying = false;

    this.storage.remove('audioSelected');

    this.getAudioFiles();
  }

  async getAudioFiles() {
    const deviceList = await this.storage.get('listaAudios') || [];
    const user = await this.storage.get('user') || {};

    const response = await this.httpService.listarGrabaciones$({}).toPromise();



    this.totalAudios = response['data']
      .filter((au) => {
        return au.id_usuario === user.id_usuario;
      }).map((au) => {
        const deviceAudio = deviceList.find((da) => da.id_grabacion === au.id_grabacion);
        return deviceAudio || au;
      }).reverse();

    this.mediaFiles = [...this.totalAudios];
  }

  recordAudio() {
    this.mediaCapture.captureAudio()
      .then(
        (data: MediaFile[]) => this.sendAudio(data),
        (err: CaptureError) => {
          if (err.code && err.code != '3') {
<<<<<<< HEAD
            console.log({err})
            this.toastService.presentToast(`Error: ${err}`);
=======
            this.toastService.presentToast('Error');
>>>>>>> e23a61d4139681abbc386403c9a447276ed9ea5f
          }
        }
      );
  }

  async sendAudio(mediaCaptureResponse: MediaFile[]) {
    this.storage.set('audioSelected', mediaCaptureResponse);
    this.navController.navigateForward('recording-form');
  }

  async openAudioOptions(event, audio) {
    event.stopPropagation();
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      translucent: true,
      event,
      componentProps: {
        'type': 'audio-options'
      }
    });
    popover.present();

    const popoverResponse = await popover.onDidDismiss();

    const action = popoverResponse['data'];
    if (action === 'share') {
      this.share(audio);
    } else if (action === 'edit') {
      this.storage.set('audioSelected', audio);
      this.navController.navigateForward('recording-form');
    }
  }

  async openFilter(event, filter) {
    event.stopPropagation();
    const popover = await this.popoverController.create({
      component: PopoverComponent,
      translucent: true,
      event,
      componentProps: {
        'type': `filter-${filter}`
      }
    });
    popover.present();

    const popoverResponse = await popover.onDidDismiss();

    const action = popoverResponse['data'];

    this.filter = {
      compas: action.compas || '',
      tonalidad: action.tonalidad || '',
      genero: action.genero || '',
    };

    if (action.genero) {
      this.mediaFiles = this.totalAudios.filter((au) => au.genero === action.genero);
    }
    if (action.tonalidad) {
      this.mediaFiles = this.totalAudios.filter((au) => au.tonalidad === action.tonalidad);
    }
    if (action.compas) {
      this.mediaFiles = this.totalAudios.filter((au) => au.compas === action.compas);
    }
  }

  searchBar($event) {
    const value = $event.detail.value;

    if (value) {
      this.mediaFiles = this.totalAudios.filter((au) => au.titulo.includes(value) || au.letra.includes(value));
    } else {
      this.mediaFiles = [...this.totalAudios];
    }
  }

  clearFilter() {

    this.mediaFiles = [...this.totalAudios];

    this.filter = {
      genero: '',
      compas: '',
      tonalidad: ''
    }
  }

  share(file) {
    this.socialSharing.share(file.nombre, null, `${environment.host}${file.ruta}`)
      .catch((res) => {
        console.log(res)
      })
  }

  async controlAudio(file, indice: number) {

    if (indice !== this.audioSelectedIndex) {

      this.audioSelectedIndex = indice;

      const audio = this.media.create(environment.host + file.ruta);

      const duration = await this.getDuration(audio);

      this.isPlaying = true;

      this.audio = { ...file, duration };

    }

  }

  getDuration(currentAudio: MediaObject): Promise<number> {
    return new Promise((resolve, reject) => {
      currentAudio.play();
      currentAudio.setVolume(0.0);
      let duration = -1;
      const interval = setInterval(() => {
        if (duration === -1) {
          duration = currentAudio.getDuration();
        } else {
          currentAudio.stop();
          currentAudio.setVolume(1.0);
          clearInterval(interval);
          resolve(duration);
        }
      }, 10);
    });
  }

  async menu() {

    this.menuController.toggle();

  }

}
